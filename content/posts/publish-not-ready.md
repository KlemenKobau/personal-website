---
title: "KRaft Kafka and Publish Not Ready"
date: 2023-07-28T17:37:22+02:00
tags: ["Kafka", "Helm", "Kubernetes"]
categories: ["troubleshooting"]
---

Today I was working with the [Bitnami Kafka helm chart](https://artifacthub.io/packages/helm/bitnami/kafka)
and noticed something strange.
I tried creating a new Kafka Cluster, where Kafka is using the new KRaft option.
This option allowes Kafka to work without Zookeeper, meaning that all setup is
handled by Kafka itself.

I setup Kafka using the following command:
```shell
helm install kafka bitnami/kafka --version 23.0.7 -f kafka.yaml
```

```yaml 
# kafka.yaml

replicaCount: 3

```

I was greeted by this.

![kafka-not-ready](/publish-not-ready/kafka-not-ready.png)

I went hunting through the logs and saw this.

```txt
[2023-07-28 16:19:39,155] WARN [RaftManager id=0] Error connecting to node kafka-2.kafka-headless.default.svc.cluster.local:9093 (id: 2 rack: null) (org.apache.kafka.clients NetworkClient)                                             │
```

Looks like there was an error while connecting.
I try to use the dnsutils pod

```shell
kubectl apply -f https://k8s.io/examples/admin/dns/dnsutils.yaml
kubectl exec -it dnsutils -- bash
```

Running nslookup I get
```txt
root@dnsutils:/# nslookup kafka-2.kafka-headless.default.svc.cluster.local
Server:         10.96.0.10
Address:        10.96.0.10#53

** server can't find kafka-0: SERVFAIL
```

I was very confused, since my other deployments with headless services were working
normally (in my case elasticsearch).
After comparing I came across this parameter `publishNotReadyAddresses`.
From the official documentation:

![docs](/publish-not-ready/kubernetes-docs.png)

It seems I have a chicken and egg problem.
Kafka cannot get ready because it cannot find other replicas,
but it cannot find other replicas,
since it is not ready.

The solution is to update the parameter in `kafka.yaml`

```yaml
# kafka.yaml

replicaCount: 3

service:
  headless:
    publishNotReadyAddresses: true
```

Seting up Kafka with this creates a working cluster:

![kafka-ready](/publish-not-ready/kafka-ready.png)

Nslookup also works as expected:

```txt
root@dnsutils:/# nslookup kafka-2.kafka-headless.default.svc.cluster.local
Server:         10.96.0.10
Address:        10.96.0.10#53

Name:   kafka-2.kafka-headless.default.svc.cluster.local
Address: 10.244.0.13
```