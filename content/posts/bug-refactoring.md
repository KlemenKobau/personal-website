---
title: "Composition over inheritance"
date: 2023-08-01T16:48:34+02:00
tags: ['bugs', 'refactoring', 'inheritance']
categories: ['bugs']
---

Today my coworker assigned me an issue.
It was about unexpected behaviour in some tested code.

The original code looked like this.

```python
class Holder:
    def first(self):
        """
        logic
        """

    def second(self):
        """
        same logic
        """
```

I refactored this to the following

```python
class Holder:
    def first(self):
        """
        logic
        """

    def second(self):
        self.first()
```

I considered this a trivial fix and paid it no heed.

The problem was, that a child class overwrote only
the `first` method, which was then also called in the
child class.

```python
class Child(Holder):
    def first(self):
        """
        new logic
        """

# second() also uses the new logic!!!
```

This is not suprising and expected,
but still tripped me up.
I am beginning to see why composition is preferred
to inheritance.
A solution to this is to extract the method like so

```python
class Holder:
    def _extracted(self):
        """
        logic
        """

    def first(self):
        self._extracted()

    def second(self):
        self._extracted()
```