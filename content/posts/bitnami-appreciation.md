---
title: "Appreciation for Bitnami helm charts"
date: 2023-07-22T19:22:03+02:00
tags: ["Masters thesis", "Kafka", "Helm", "Bitnami"]
categories: ["Masters thesis"]
---

While working on my masters thesis I took a look at
the parameters available for the Bitnami Kafka helm chart.
The amount of available options eye opening for someone,
that is just starting to use Kafka.

I saw that there was a new way to use Kafka called KRaft,
that no longer requires a Zookeeper instance, which I will
promptly put to good use in my masters thesis.

I saw that the helm chart also provides an option to
bootstrap topics.
Previously I was using the Strimzi Kafka operator,
however I was always concerned CRDs, since I may not always have
the required rights to create them on clusters.
Not anymore!

I also saw that they are creating a helm chart for Opensearch,
which I will probably also use to replace the Opster opensearch operator.