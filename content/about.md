---
title: "About"
date: 2023-07-28T17:37:22+02:00
hideMeta: true
---

**Email**: [klemen.kobau@gmail.com](mailto:klemen.kobau@gmail.com)

**Location**: Ljubljana, Slovenia

**Hobbies**: Kendo, programming

Currently working on my masters thesis. 
I am interested in low level programming, virtualisation (Containers, Kubernetes, QEMU, KVM) and
backend development. 

I plan to dabble a bit with 3D printing, because I want to:
- [ ] print my own drone
- [ ] print pathfinder miniatures for my games

I am currently learning:
- Rust
- Kubernetes
- Kafka

I am currently playing:
- pathfinder
- factorio